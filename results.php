
<?php
include_once('include/variables.php');
include_once('include/class.db_connection.php');
include_once('include/class.settings.php');
include_once('include/class.ratings.php');

$connection = new connection();
$connection = $connection -> db_connect();

$settings = new settings();
$id = $settings -> getCurrentQuestion($connection);

$ratings = new ratings();
$results = $ratings -> getResults($connection, $id);


//avoiding division by zero
($total = $results['negative'] + $results['neutral'] + $results['positive']) != 0 ?: $total = 1;

//calculate the percantage of people who voted for each choice, format it to 2 decimals
$negPercent = number_format((($results['negative'] / $total) * 100), 2);
$neutPercent = number_format((($results['neutral'] / $total) * 100), 2);
$posPercent = number_format((($results['positive'] / $total) * 100), 2);

//Check whether to print person or people in results
function peopleOrPerson($amount) {
    return $amount != 1 ? "$amount people" : "$amount person";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">
    <meta name="author" content="Leevi Lappalainen">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="contentContainer">
        <p>"<?php echo($results['question']); ?>"</p>
        <?php
        $html = "<p>".peopleOrPerson($results['negative'])." ($negPercent%) voted &#128577</p>
                <br> <p>".peopleOrPerson($results['neutral'])." ($neutPercent%) voted &#128528</p>
                <br> <p>".peopleOrPerson($results['positive'])." ($posPercent%) voted &#128578";
        echo($html);
        ?>
        <br>
        <a href="./index.php">Return to voting</a>
        <br>
        <a href="login.php">Admin</a>
    </div>
    <div class="footer">
    <p><a href="https://gitlab.com/FerrisU/rating-system">Rating system</a> by Leevi Lappalainen, graphics by Sini Kivelä, 2022</p>
    </div>
</body>