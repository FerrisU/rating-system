<?php
    include_once('include/variables.php');
    include_once('include/class.db_connection.php');
    include_once('include/class.settings.php');
    include_once('include/class.ratings.php');

    $connection = new connection();
    $connection = $connection->db_connect();

    $settings = new settings();

    //Check if we're already logged in. If not, check if there was a password provided, then check it.
    if ($settings->logged($connection) != "true") {
        if (isset($_POST['adminPasswd'])) {
            $passwdCheck = $settings->passwdCheck($connection, $_POST['adminPasswd']);
            if ($passwdCheck) {
                $settings->loginAction($connection, 'true');
            } else {
                $settings->loginAction($connection, 'false');
                header('location:login.php?check=failed');
            }
        } else {
            header('location:login.php?check=failed');
        }
    } 

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">
    <meta name="author" content="Leevi Lappalainen">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="contentContainer">
        <a href="questionInterface.php">Add a new question</a>
        <hr>
        <a href="chooseQuestion.php">Select another question</a>
        <hr>
        <a href="results.php">Results</a>
        <hr>
        <a href="changePassword.php">Change the admin password</a>
        <hr>
        <a href="index.php">Return to voting</a>
    </div>
</body>
<script>
</script>
</html>