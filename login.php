<?php
include_once('include/variables.php');
include_once('include/class.db_connection.php');
include_once('include/class.settings.php');
include_once('include/class.ratings.php');

$connection = new connection();
$connection = $connection->db_connect();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">
    <meta name="author" content="Leevi Lappalainen">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="contentContainer">
        <?php
            if (isset($_GET['check'])) {
                echo('The password was incorrect or you must log in again');
            }            
        ?>
        <form action="admin.php" method="POST">
            <input type="password" placeholder="Password" name="adminPasswd" id="adminPasswd" class="form-control">
            <button type="submit" class="btn">Submit</button>
        </form>
    </div>
    <div class="footer">
        <p><a href="https://gitlab.com/FerrisU/rating-system">Rating system</a> by Leevi Lappalainen, graphics by Sini Kivelä, 2022</p>
    </div>
</body>
<script>
</script>
</html>