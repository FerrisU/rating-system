
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">
    <meta name="author" content="Leevi Lappalainen">
    <link rel="stylesheet" href="style.css">
</head>

<?php
    include_once('include/variables.php');
    include_once('include/class.db_connection.php');
    include_once('include/class.settings.php');
    include_once('include/class.ratings.php');
    
    $settings = new settings();
    if ($settings->logged($connection) != "true") {
        header('location:login.php');
    }

    $connection = new connection();
    $connection = $connection->db_connect();

    $ratings = new ratings();
    $questions = $ratings->getAllQuestions($connection);

?>

<body>
    <div class="contentContainer">
        <div>
        <form id="questionForm" action="setQuestion.php" method="POST">
        <input name="chosenQuestion" id="chosenQuestion" hidden="hidden">
        <?php
            //Printing each question in a button
            $counter = 0;
            foreach($questions as $q) {
                $html = "<button onclick='questionChosen(this);' id=".$q['id']." class='qbtn btn btn-lg btn-outline-primary'>
                        ".$q['question']." <br>
                        ".$q['negative']." &#128577,
                        ".$q['neutral']." &#128528,
                        ".$q['positive']." &#128578
                        </button>";
                if($counter == 2) {
                    $counter = 0;
                    $html .= "<br>";
                } else {
                    $counter++;
                }
                echo($html);
            }
        ?>
        </form>
        </div>
        <a href="admin.php">Admin</a>
    </div>
</body>

<script>
    function questionChosen(ele) {
        var qID = ele.id;
        document.getElementById("chosenQuestion").value = qID;
    }
</script>



