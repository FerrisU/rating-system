<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">
    <meta name="author" content="Leevi Lappalainen">
    <link rel="stylesheet" href="style.css">
</head>



<body>
<div class="contentContainer">

<?php

include_once('include/variables.php');
include_once('include/class.db_connection.php');
include_once('include/class.settings.php');
include_once('include/class.ratings.php');

//Check what choice was made
//This sucks, but it will do, maybe a better implementation later?
if (isset($_POST['negative_x'])) {
    $mood = "negative";
} elseif (isset($_POST['neutral_x'])) {
    $mood = "neutral";
} elseif (isset($_POST['positive_x'])) {
    $mood = "positive";
}

$connection = new connection();
$connection = $connection->db_connect();

$settings = new settings();
$id = $settings -> getCurrentQuestion($connection);

$ratings = new ratings();

if (isset($mood)) {
    $rating = $ratings->updateRatings($connection, $id, $mood);
    if ($rating) {
        echo("Thank you for your input!");
    } else {
        echo("Error updating ratings");
    }
} else {
    echo("Unknown error!");
}

?>

</div>
<div class="footer">
    <p><a href="https://gitlab.com/FerrisU/rating-system">Rating system</a> by Leevi Lappalainen, graphics by Sini Kivelä, 2022</p>
</div>
<body>

<script>
    //Take us back to the rating page after a set time.
    const timeout = setTimeout(redirect, <?php echo($returnTime) ?>);
    function redirect() {
        location.assign("<?php echo $url ?>")
    }
</script>