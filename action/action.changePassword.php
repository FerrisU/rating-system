<?php
    include_once('../include/class.db_connection.php');
    include_once('../include/class.settings.php');
    include_once('../include/variables.php');

    $oldPasswd = $_POST['oldPasswd'];
    $newPasswd = $_POST['newPasswd'];

    $connection = new connection();
    $connection = $connection->db_connect();

    $settings = new settings();
    if ($settings->passwdCheck($connection, $oldPasswd)) {
        if($newPasswd != '' && $newPasswd != NULL) {
            $result = $settings->changePasswd($connection, $newPasswd);
        } else {
            header('location:../changePassword.php?invalid=true');
        }
    } else {
        header('location:../changePassword.php?check=failed');
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">
    <meta name="author" content="Leevi Lappalainen">
    <link rel="stylesheet" href="../style.css">
</head>
<body>
<div class="contentContainer">
        <?php
            if ($result) {
                echo("Password was changed successfully. You will soon be redirected. Alternatively, click on the link below");
            } else {
                header('location:../changePassword.php?error=true');
            }
        ?>
        <a href="admin.php">Admin</a>
    
</div>
<div class="footer">
    <p><a href="https://gitlab.com/FerrisU/rating-system">Rating system</a> by Leevi Lappalainen, graphics by Sini Kivelä, 2022</p>
</div>
</body>
<script>
    //Take us back to the rating page after a set time.
    const timeout = setTimeout(redirect, <?php echo($returnTime) ?>);
    function redirect() {
        location.assign("<?php echo $url ?>")
    }
</script>
</html>