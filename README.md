Simple kiosk style feedback/rating system with negative, neutral, and positive emojis.

<h3>Options</h3>
You can find and change a variety of settings in the "variables.php" file. These include integral database settings, and some customization options not available through the GUI.

<h3>Setup</h3>
Setup of the rating system has been made significantly simpler, just import the `ratingsystem.sql` file and modify `/include/variables.php` to your settings. The default password is "password", and can be changed through the admin page.

<h3>Screenshots</h3>
![Picture of the voting UI](pictures/Screenshot_from_2023-02-18_13-33-40.png)
<hr>
![Picture of the results page](pictures/Screenshot_from_2023-02-18_13-33-48.png)