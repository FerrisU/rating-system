<?php
    //DB connection settings! you will probably want to change these
    $host = "localhost";
    $user = "root";
    $password = "";
    $db = "ratingsystem";

    //general settings
    //The location of the rating page.
    $url = "http://localhost/rating-system/index.php";

    //Customization settings
    //Whether or not to show the link to the results page
    $showResultsLink = true;
    //time it takes until returning back to the voting page, in milliseconds
    $returnTime = 2500;
    //Choose whether or not to show the question/name for the rating.
    //If false, $hardCQuestion will be used, which can be set to blank if you want no question to be shown
    $showQuestion = true;
    $hardCQuestion = "";
