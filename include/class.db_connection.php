<?php

class connection {

    public function db_connect() {
        require('variables.php');
        $connection = mysqli_connect($host, $user, $password, $db);
        return($connection);
    }
}