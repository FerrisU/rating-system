<?php
    class settings {
        
        //returns the ID of the currently selected question
        public function getCurrentQuestion($conn) {
            $sql = "SELECT `value` FROM settings WHERE setting = 'currentQuestion'";
            if ($result = mysqli_query($conn, $sql)) {
                while ($row = $result->fetch_object()) {
                    $questionId = $row->value;
                }
                return($questionId);
            } else {
                return("Error fetching current question");
            }
        }

        //Changes the currently selected question
        public function setQuestion($conn, $id) {
            $sql = $conn->prepare("UPDATE settings SET `value` = ? WHERE setting = 'currentQuestion'");
            $sql->bind_param('s', $id);
            return($sql->execute());
        }

        public function passwdCheck($conn, $passwd) {
            $sql = "SELECT `value` FROM settings WHERE setting = 'adminPassword'";
            if ($result = mysqli_query($conn, $sql)) {
                while ($row = $result->fetch_object()) {
                    $hashed_passwd = $row->value;
                }
                if (password_verify($passwd, $hashed_passwd)) {
                    return(true);
                } else {
                    return(false);
                }
            } else {
                return("There was an error logging in.");
            }
        }

        //Changes the admin password
        public function changePasswd($conn, $newPasswd) {
            $newPasswd = password_hash($newPasswd, PASSWORD_DEFAULT);
            $sql = $conn->prepare("UPDATE settings SET `value` = ? WHERE setting = 'adminPassword'");
            $sql->bind_param('s', $newPasswd);
            return($sql->execute());
        }

        //Check if we're logged in
        //This will also return false if it's unable to get the result for some reason
        public function logged($conn) {
            $sql = "SELECT `value` FROM settings WHERE setting = 'loggedIn'";
            $result = mysqli_query($conn, $sql);
            $result = $result->fetch_row()[0] ?? false;
            return($result);
        }

        //Mark us as logged in or out
        public function loginAction($conn, $bool) {
            $sql = $conn->prepare("UPDATE settings SET `value` = ? WHERE setting = 'loggedIn'");
            $sql->bind_param('s', $bool);
            $sql->execute();
        }

    }