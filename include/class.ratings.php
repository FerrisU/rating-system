<?php

class ratings {

    //Get the question from the ratings table
    public function getQuestion($conn, $id) {
        $sql = "SELECT `name` FROM ratings WHERE id = $id";
        $result = mysqli_query($conn, $sql);

        $question = $result->fetch_row()[0] ?? false;

        if ($result) {
            return($question);
        } else {
            return("Error fetching question data");
        }
    }

    //Add 1 to selected option
    public function updateRatings($conn, $id, $mood) {
        $sql = "UPDATE ratings SET $mood = $mood + 1 WHERE id = $id";
        return(mysqli_query($conn, $sql));
    }

    //Get the results for the selected question
    public function getResults($conn, $id) {
        $sql = "SELECT * FROM ratings WHERE id = $id";
        $return = [];
        if ($result = mysqli_query($conn, $sql)) {
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_object()) {
                    $return['question'] = $row->name;
                    $return['negative'] = $row->negative;
                    $return['neutral'] = $row->neutral;
                    $return['positive'] = $row->positive;
                }
                return($return);
            }
        } else {
            return("Error fetching results");
        }
    }

    //Inserting a new question
    public function insertQuestion($conn, $question) {
        //Injection protection
        $sql = $conn->prepare("INSERT INTO ratings (`name`, negative, neutral, positive) VALUES (?, 0, 0, 0)");
        $sql->bind_param('s', $question);
        return($sql->execute());
    }

    //get all questions and their votes
    public function getAllQuestions($conn) {
        $sql = 'SELECT * FROM ratings';
        $questions = [];
        if ($result = mysqli_query($conn, $sql)) {
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_object()) {
                    $questions[$row->id]['id'] = $row->id;
                    $questions[$row->id]['question'] = $row->name;
                    $questions[$row->id]['negative'] = $row->negative;
                    $questions[$row->id]['neutral'] = $row->neutral;
                    $questions[$row->id]['positive'] = $row->positive;
                }
                return($questions);
            } else {
                return("No questions found");
            }
        } else {
            return("There was an error fetching questions");
        }
    }

}