<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">
    <meta name="author" content="Leevi Lappalainen">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="contentContainer">
        <?php
            if (isset($_GET['check'])) {
                echo('Old password is incorrect');
            } else if (isset($_GET['invalid'])){
                echo('New password is invalid, please try again');
            }
        ?>
        <form action="action/action.changePassword.php" method="POST">
            <input type="text" placeholder="Current password" name="oldPasswd" id="oldPasswd" class="form-control">
            <input type="text" placeholder="New password" name="newPasswd" id="newPasswd" class="form-control">
            <button type="submit" class="btn">Submit</button>
        </form>
    </div>
    <div class="footer">
        <p><a href="https://gitlab.com/FerrisU/rating-system">Rating system</a> by Leevi Lappalainen, graphics by Sini Kivelä, 2022</p>
    </div>
</body>
</html>