<?php
include_once('include/class.settings.php');

$settings = new settings();

if ($settings->logged($connection) != "true") {
    header('location:login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>New question</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">
    <meta name="author" content="Leevi Lappalainen">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="contentContainer">
        <form action="addQuestion.php" method="POST">
            <label for="questionName">Question name</label>
            <input type="text" name="questionName" id="questionName" class="form-control">
            <br>
            <button type="submit" class="btn btn-primary">Add new question</button>
        </form>
        <a href="admin.php">Admin</a>
    </div>
</body>
</html>