<?php
include_once('include/variables.php');
include_once('include/class.db_connection.php');
include_once('include/class.settings.php');
include_once('include/class.ratings.php');

$connection = new connection();
$connection = $connection->db_connect();

$settings = new settings();
$id = $settings->getCurrentQuestion($connection);

$ratings = new ratings();
$question = $ratings->getQuestion($connection, $id);

//Making sure that we are logged out when returning to the voting page
$settings->loginAction($connection, 'false');

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">
    <meta name="author" content="Leevi Lappalainen">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="contentContainer">
        <div class="button-group mx-auto">
            <?php /** Question */ echo($showQuestion == true ? '<p>'.$question.'</p>' : '<p>'.$hardCQuestion.'</p>') ?>
            <form id="ratingForm" action="./rating.php" method="POST">
                <input type="image" src="./graphics/vihainen_hymio.png" alt="Negative" id="negative" name="negative" class="btn btn-lg imgBtn">
                <input type="image" src="./graphics/neutral_hymio.png" alt="Neutral" id="neutral" name="neutral" class="btn btn-lg imgBtn">
                <input type="image" src="./graphics/ilo_hymio.png" alt="Positive" id="positive" name="positive" class="btn btn-lg imgBtn">
            </form>
            <br>
            <?php /** Results link */ if ($showResultsLink == true){ echo('<a href="./results.php">View results</a>'); } ?>
        </div>
    </div>
    <div class="footer">
        <p><a href="https://gitlab.com/FerrisU/rating-system">Rating system</a> by Leevi Lappalainen, graphics by Sini Kivelä, 2022</p>
    </div>
</body>
<script>
</script>
</html>