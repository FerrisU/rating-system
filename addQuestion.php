<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">
    <meta name="author" content="Leevi Lappalainen">
    <link rel="stylesheet" href="style.css">
</head>

<?php
include_once('include/variables.php');
include_once('include/class.db_connection.php');
include_once('include/class.settings.php');
include_once('include/class.ratings.php');

$connection = new connection();
$connection = $connection->db_connect();

//Take us back to the login page if we're not logged in
$settings = new settings();
if ($settings->logged($connection) != "true") {
    header('location:login.php');
}

$ratings = new ratings();
$result = $ratings->insertQuestion($connection, $_POST['questionName']);

if ($result) {
    echo("Question added successfully!");
} else {
    echo("There was an error adding your question");
}

?>

<script>
    //Take us back to the rating page after a set time.
    const timeout = setTimeout(redirect, <?php echo($returnTime) ?>);
    function redirect() {
        location.assign("./admin.php")
    }
</script>
