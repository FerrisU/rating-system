
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">
    <meta name="author" content="Leevi Lappalainen">
    <link rel="stylesheet" href="style.css">
</head>

<body>
        <?php
            include_once('include/class.settings.php');
            
            if ($passwdCheck) {
                $settings->loginAction($connection, 'true');
            } else {
                header('location:login.php');
            }
        ?>
    <div class="contentContainer">
        <a href="questionInterface.php">Add a new question</a>
        <hr>
        <a href="chooseQuestion.php">Change the current question</a>
        <hr>
        <a href="results.php">Return to results</a>
    </div>
</body>